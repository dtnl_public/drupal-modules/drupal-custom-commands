


class Message:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @classmethod
    def fail(self, message):
        print(self.FAIL + message + self.ENDC)

    @classmethod
    def warning(self, message):
        print(self.WARNING + message + self.ENDC)

    @classmethod
    def success(self, message):
        print(self.GREEN + message + self.ENDC)

    @classmethod
    def advice(self, message):
        print(self.BLUE + message + self.ENDC)
        