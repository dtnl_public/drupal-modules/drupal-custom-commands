import config, time

from classes.TemplateManager import TemplateManager
from classes.TemplateTranspiler import TemplateTranspiler


def main():

    templateManager = TemplateManager()
    templates = templateManager.get_templates()

    start = time.time()

    for template in templates:
        TemplateTranspiler.transpile(template, templates)

    print("Finished transpiling in %s seconds" % str(time.time() - start))
    
    return '0'

main()

    





    