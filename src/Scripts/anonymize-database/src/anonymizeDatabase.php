<?php

// Scrub user data.
$io->section('Anonymising user emails and passwords');

$io->writeln('Blocking external users and updating their emails and passwords.');

$database->update('users_field_data')
    ->condition('mail', '%'.$excludes.'%', 'NOT LIKE')
    ->condition('mail', $excludedEmails , 'NOT IN')
    ->condition('uid', self::ANONYMOUS_USER_ID, '<>')
    ->fields([
        'status' => self::INACTIVE_USER_STATUS,
        'pass' => $pass,
    ])
    ->expression('name', "CONCAT('user+', uid, '@email.cancelled')")
    ->expression('mail', "CONCAT('user+', uid, '@email.cancelled')")
    ->expression('init', "CONCAT('user+', uid, '@email.cancelled')")
    ->execute();

$io->writeln('Updating dept users.');

$database->update('users_field_data')
    ->fields([
        'pass' => $pass,
    ])
    ->execute();

// Scrub queue items.
$io->section('Clearing queues');
$database->truncate('queue')->execute();
$io->writeln('Truncated {queue} table');

// Truncate cache_entity table to reflect changes immediately.
$io->section('Clearing cache entity');
$database->truncate('cache_entity')->execute();
$io->writeln('Truncated {cache_entity} table');

$io->newline(2);
$io->success("Success: Operation terminated!");

$this->logger()->success(dt('database anonymized'));
