<?php

namespace Drupal\custom_drush_commands\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;
use Drupal\Core\Site\Settings;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class CustomDrushCommands extends DrushCommands
{

    private $base_dir;
    private $front_end_path;
    private $back_end_path;

    const ANONYMOUS_USER_ID = 0;
    const INACTIVE_USER_STATUS = 0;


    public function __construct()
    {
        $this->base_dir = dirname(__DIR__, 1) . '/scripts/';
        $this->front_end_path =  dirname(__DIR__, 7) . $this->validatePath(Settings::get('frontend_path'));
        $this->back_end_path = dirname(__DIR__, 7) . $this->validatePath(Settings::get('backend_path'));
    }

    /**
     * Usually run this script after getting a production database.
     * When the script runs, it will sanitise / scrub the database
     * by updating all user emails, except the "deptagency.com" ones
     * and the one that belongs to the user that was provided as an argument.
     * e.g. "test@example.com" will be updated to "test@email.cancelled".
     * The script will also update all user passwords to "pass".
     *
     * How to run the script:
     * Adjust path to script based on your local environment.
     * SCRUB_DB_SCRIPT_PATH="/home/xenovkl/projects/docs"
     * drush scr ${SCRUB_DB_SCRIPT_PATH}/scrub-database.drush dev='admin'
     *
     * If no dev parameter is set, the admin user will be used instead.
     *
     * @option email_name
     *  default is admin but you can make this into anything you want.
     *
     * @option excludes
     *  excludes a certain domain from being anonymized. enter a string seperated by commas
     *
     * @option excluded_emails
     *  excludes a certain email from being anonymized. enter a string seperated by commas
     *
     * @command custom_drush_commands:anonymize-database
     * @aliases directory
     *
     */
    public function anonymizeDatabase($options = ['email_name' => 'admin', 'exclude_domain' => 'deptagency', 'excluded_emails' => ''])
    {
        // load the super user
        $user = User::load(1);

        // Load the necessary command services.
        $input = $this->input();
        $io = $this->io();

        // Load the necessary drupal services.
        $database = \Drupal::service('database');
        $password_hasher = \Drupal::service('password');

        //defines the name before the @ sign
        $emailName = $options['email_name'];

        // exclude these domains from being anonymized
        $excludes = $options['exclude_domain'];

        // excludes these specific emails from being anonymized
        $excludedEmails = explode(',', $options['excluded_emails']);

        $pass = $password_hasher->hash('test');

        include_once(dirname(__DIR__, 1) . '/scripts/anonymize-database/src/anonymizeDatabase.php');
    }

    /**
     * Command description here.
     *
     * @param array $options
     *   An associative array of options whose values come from cli, aliases, config, etc.
     * @usage custom_drush_commands-parse-nunjucks directory
     *   Use to parse nunjucks to twig. use the directory argument to specify
     *   where the twig templates need to go.
     *
     * @command custom_drush_commands:parse-nunjucks
     * @aliases directory
     */
    public function parseNunjucksToTwig( $options = ['option-name' => 'default'] )
    {

        // Check if python version is compatible for this script.
        if (!$this->pythonVersionIsCompatible())
            exit;

        if ( Settings::get('frontend_path') && Settings::get('backend_path') ) {
            $command = escapeshellcmd(
                'python ' . $this->base_dir .'syntax-converter/src/main.py ' . $this->front_end_path .' '. $this->back_end_path
            );

            // Script returns '0' if it reaches the end / ran successfully.
            $scriptDidNotFail = shell_exec($command);

            if ($scriptDidNotFail) {
                $this->logger()->success(dt('Nunjucks parsed successfully.'));
            } else {
                $this->logger()->error(dt('Whoops, script did not finish. Something went wrong.'));
            }

        } else {
            $this->logger()->error(dt('No front-end or back-end path set. Make sure to set these settings in the setting.local.php file.'));
            $this->logger()->notice(dt('Set them relative from <project>/src/'));
        }
    }

    private function validatePath($path) {
        return '/' . ltrim($path, '/');
    }

    // 'python' command must be Python 3 or higher.
    private function pythonVersionIsCompatible() {
        $versionString = shell_exec("python --version");

        if (strpos($versionString, 'Python 3') !== false)
            return true;

        $this->logger()->error(dt("Please set the 'python' command to Python version 3 or higher"));
        $this->logger()->notice(dt("Do not change the version by creating an alias in .bash_profile, do it by creating a symlink."));
        $this->logger()->notice(dt("For Unix based systems this might work if Python 3 is installed: ln -s /usr/local/bin/python3 /usr/local/bin/python"));

        return false;
    }

}
